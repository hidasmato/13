import { func, con } from './func.mjs'
import * as many from './many.mjs';
import myDefFunc, { a } from './defExport.mjs';
import {smallWord} from './importFromME.mjs';

console.log("Экспорт функции:")
for (let i = -2; i < 5; i++)
    console.log("2^(" + (i < 0 ? i : " " + i) + ")=" + func(i));
console.log("Экспорт константы", con);

console.log("All in many is:", Object.keys(many).toString());

myDefFunc();
console.log("Экспорт константы в дополнение к дефолтному экспорту", a);

console.log("Иморт через два экспорта: ");
smallWord();